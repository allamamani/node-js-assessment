const mongoose = require('mongoose');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const router = require('express').Router();
const Promise = require('bluebird')
var auth = require('../auth');
var utils = require('../../cmnfun/utils');
var nodeMailer = require('../../config/nodemailer');

const Users = mongoose.model('Users');

router.post('/register', auth.optional, (req, res) => {
    const user = req.body

    if (!user.email) {
        return res.status(422).json({
            errors: {
                message: res.__('Email is required'),
            },
        });
    }
    if (!utils.validateEmail(user.email)) {
        return res.status(422).json({
            errors: {
                message: res.__('Please enter valid Email Id'),
            },
        });
    }
    Users.find({ email: user.email }, (err, result) => {
        if (result.length > 0) {
            return res.status(422).json({
                errors: {
                    message: 'Email Id already registered,try with different Email Id',
                },
            });
        } else {
            user.email = user.email.toLowerCase();
            // let newuser = new Users({
            //     fullName: user.fullName,
            //     email: user.email,
            //     jobTitle: user.jobTitle,
            //     department: user.department,
            //     location: user.location,
            //     age: user.age,
            //     salary: user.salary
            // })
            const newuser = new Users(user);
            newuser.setPassword(user.password)
            newuser.save((err, success) => {
                if (err) {
                    res.json(err)
                } else {
                    newuser.token = newuser.generateJWT();
                    let users = newuser.toAuthJSON()
                    return res.json({ user: users });
                    // return res.json({
                    //     message: res.__('User created'),
                    // });
                }
            })
        }
    })
})

//SignIn with Password 
router.post('/login', auth.optional, (req, res, next) => {
    const body = req.body;
    if (!body.username) {
        return res.status(422).json({
            errors: {
                message: 'Email Id is required'
            },
        });
    }
    if (!body.password) {
        return res.status(422).json({
            errors: {
                message: 'Password is required'
            },
        });
    }
    Users.findOne({ email: body.username }, (err, user) => {
        if (user) {
            if (user.salt) {
                if (user && user.validatePassword(body.password)) {
                    Users.updateOne({ _id: user._id }, { $set: { lastLoginDate: new Date() } }, (err, updated) => {
                        user.token = user.generateJWT();
                        let users = user.toAuthJSON()
                        return res.json({ user: users });
                    })
                } else {
                    return res.status(422).json({
                        errors: {
                            message: 'Invalid Login Credientials'
                        },
                    });
                }
            } else {
                return res.status(422).json({
                    errors: {
                        message: 'Invalid Login Credientials'
                    },
                });
            }
        } else {
            return res.status(422).json({
                errors: {
                    message: 'Invalid Login Credientials'
                },
            });
        }
    })
})

// get users list
router.get('/userList', auth.required, (req, res) => {
    Users.find({}, (err, userList) => {
        if (err) {
            return res.status(422).json({
                errors: {
                    message: res.__('Internal Server Error'),
                },
            });
        } else {
            return res.json({ user: userList });
        }
    })

})

//Update
router.post('/update', auth.required, (req, res) => {
    Users.updateOne({ _id: req.payload.id }, { $set: { salary: req.body.salary } }, (err, update) => {
        if (err) {
            return res.status(422).json({
                errors: {
                    message: res.__('Internal Server Error'),
                },
            });
        } else {
            return res.json({ message: "User datails updated successfully" });
        }
    })
})

//Delete
router.post('/Delete', auth.required, (req, res) => {
    Users.updateOne({ _id: req.payload.id }, { $set: { expiryDate: new Date() } }, (err, update) => {
        if (err) {
            return res.status(422).json({
                errors: {
                    message: res.__('Internal Server Error'),
                },
            });
        } else {
            return res.json({ message: "User deleted successfully" });
        }
    })
})

//Forgot Password
router.post('/forgotPassword', auth.optional, (req, res, next) => {
    const user = req.body
    if (!user.email) {
        return res.status(422).json({
            errors: {
                message: 'Email Id is required'
            },
        });
    }
    if (user.email) {
        if (!utils.validateEmail(user.email)) {
            return res.status(422).json({
                errors: {
                    message: 'Please enter valid Email Id'
                },
            });
        }
        Users.find({ email: user.email }, (err, doc) => {
            if (err) {
                return res.status(422).json({
                    errors: {
                        message: 'Email Id  not registered'
                    }
                });
            }
            if (doc.length > 0) {
                Users.find({ _id: doc[0]._id }, (err, val) => {
                    if (err) {
                        return res.status(500).json({
                            errors: {
                                message: 'Internal Server Error'
                            }
                        });
                    } else {
                        let token = jwt.sign({ id: doc[0]._id }, process.env.SECRET, { expiresIn: '5 minutes' });
                        let url = 'URL/forgotPassword?token=' + token + '&id=' + doc[0]._id;
                        Users.updateOne({ _id: doc[0]._id }, { $set: { passwordToken: token } }, (err, update) => {
                            if (err) {
                                return res.status(422).json({
                                    errors: {
                                        message:'Some error occured, please retry again'
                                    }
                                })
                            } else {
                                var mailOptions = {
                                    to: user.email,
                                    from: "allamamani1994@gmail.com",
                                    subject: "Reset your password",
                                    html: ''
                                }
                                nodeMailer(mailOptions).then((err,result) => {
                                    if (result) {
                                        reslv("Email Sent")
                                        return res.json({token:token, message: "Password reset mail Sent" });
                                    } else {
                                        rej(err)

                                        return res.status(422).json({
                                            errors: {
                                                message: 'Some error occured, please retry again'
                                            }
                                        });
                                    }
                                })
                            }
                            // return res.json({ message: "Rest password link sended" })
                        })


                    }
                })
            } else {
                return res.status(422).json({
                    errors: {
                        message:'Email Id not registered',
                    }
                });
            }
        })

    } else {
        return res.status(422).json({
            errors: {
                message: res.__('Email Id not registered'),
            }
        });
    }
})

//After get reset password link set password
router.post('/resetPassword',auth.required, (req, res) => {
    const user = req.body
    const id = req.payload.id
    Users.findOne({ _id: id }, (err, details) => {
        if (err) {
            return res.status(500).json({
                errors: {
                    message: 'Internal Server Error'
                }
            });
        } else {
            let token = null;
            if (req.headers.authorization === details.passwordToken) {
                token = req.headers.authorization;
                jwt.verify(token, process.env.SECRET, (err, decoded) => {
                    if (err) {
                        return res.status(422).json({
                            errors: {
                                code: 'forgot_token_expired',
                                message: err.message,
                                error: {},
                            },
                        });
                    } else {
                        const finalUser = new Users(user);
                        // finalUser.passwordToken = null;
                        finalUser.setPassword(user.password);
                        return finalUser.updateOne(finalUser, (err, doc) => {
                            if (err) {
                                return res.status(500).json({
                                    errors: {
                                        message:'Internal Server Error'
                                    }
                                });
                            }
                            res.json({ message:"Password Changed Successfully" })
                        });
                    }
                })
            } else {
                return res.status(500).json({
                    errors: {
                        message: 'some error occured'
                    }
                });
            }
        }
    })
})


module.exports = router;
