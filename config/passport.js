const mongoose = require('mongoose');
const passport = require('passport');
const LocalStrategy = require('passport-local');

const Users = mongoose.model('Users');

passport.use(new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
}, (username, password, done) => {
    let isEmail = username.includes("@");
    if (isEmail) {
        Users.findOne({ email: username }, (err, user) => {
            if (user) {
                if (user.salt) {
                    if (user && user.validatePassword(password)) {
                        return done(null, user);
                    } else {
                        return done(null, false, {
                            errors: {
                                message: "Email or password is invalid"
                            }
                        });
                    }
                } else {
                    return done(null, false, {
                        status: 1,
                        user: user,
                        message: "Registration Not Complete..Complete Registration Process"
                    });
                }
            } else {
                return done(null, false, {
                    errors: {
                        message: "Emailor password is invalid"
                    }
                });
            }
        })
    }
    else {
        return done(null, false, {
            errors: {
                message: "Email or password is invalid"
            }
        });
    }
}))