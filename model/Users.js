const mongoose = require('mongoose')
const jwt = require('jsonwebtoken');
const crypto = require('crypto');

const { Schema } = mongoose;

const UsersSchema = new Schema({
    email:{type:String,unique:true,sparse:true},
    hash: { type: String },
    salt: { type: String },
    fullName:{type:String},
    jobTitle:{type:String},
    department:{type:String},
    location:{type:String},
    age:{type:Number},//0-female , 1-male
    salary:{type:Number},
    passwordToken:{type:String},
    createdDate: { type: Date, default: new Date() },
    expiryDate: { type: Date },
    lastLoginDate:{type:Date},
})

UsersSchema.methods.setPassword = function (password) {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};

UsersSchema.methods.validatePassword = function (password) {
    const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
    return this.hash === hash;
};

UsersSchema.methods.generateJWT = function () {
    return jwt.sign({
        id: this._id,
    }, process.env.SECRET, { expiresIn: '30 days' });
}

UsersSchema.methods.toAuthJSON = function () {
    return {
        _id: this._id,
        fullName: this.fullName,
        email:this.email,
        token: this.token,
    };
};
mongoose.model('Users', UsersSchema)