const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const dotenv = require('custom-env');

var http = require("http");

dotenv.env();

const app = express();

app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 1000000 }))
app.use(bodyParser.json({ limit: '50mb', parameterLimit: 1000000 }))
app.use(bodyParser.text())
app.use(bodyParser.raw())

mongoose.Promise = global.Promise;


mongoose.connect(process.env.DB_URL, { useCreateIndex: true, useNewUrlParser: true })
    .then(() => {
        console.log("mongodb connected successfully");
    })
    .catch(err => {
        console.log(err);

    })
mongoose.set('debug', true);

// Models and Routes
require('./model/Users');

require('./config/passport');
// require('./config/nodemailer');

app.use(require('./routes/apis/users'));

app.get('/', function (req, res, next) {
    res.send({
        started: res.__("Started at : ") + started,
        date: new Date(),
    });
});

server = http.createServer(app);
server.listen(process.env.PORT, () => console.log(`Server is running on port ${process.env.PORT}`));

